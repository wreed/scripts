Will Reed's scripts
======

These are my scripts that I've written for myself and others on Linux.
Not all of them are useful to everyone, but I will list a few below
that I believe others could make great use of.

> ## *screenshot*
 ```
A script for people like me who find themselves
hopping between Wayland & X; this script uses the 
$XDG_SESSION_TYPE environment variable to check
if you're on X or Wayland, then uses corresponding
tools for it. It has command line flags for a select
option and a wait option for giving a timeout before
capturing the screen. It also will send a desktop
notification to you with a thumbnail of the captured
image. Currently it supports Shotgun/Hacksaw for X
and Grim/Slurp for Wayland. I plan to add other
tools into it in the future; i.e. Scrot, Maim, etc.
```

> ## *editwmconfig*
```
Reads the $DESKTOP_SESSION environment variable to find
which window manager is in use- and will open $EDITOR
(or nano as a fallback) to open the configuration file
for the WM in use. NOTE: the configuration files are
defined in the script by the user (I have put in the
ones that I personally use.
```

> ## *killcasc*
```
Short for "kill cascade", I made it for when a program
is being stubborn and will not respond to the default
kill signal (SIGINT). It will check to see if the
process is still running and attempt to kill it
with these signals in order:
SIGINT |> SIGQUIT |> SIGTERM |> SIGKILL
you just give it the name of the program you are
trying to kill, i.e. "killcasc firefox"
```
> ## *ifnotrun*
```
I made this for use with window manager autostart
scripts, to launch things like a polkit, notification
daemon, X compositor, 3rd party status bar, or
wallpaper setting tools that dont instantly exit to
the background. It attempts to run the program and
push it to the background. It has a --exec option
for first running a process, and --kill to kill
and relaunch something, and --opts to pass options
directly to the target program.
```

> ## *volume & brightness*
```
These scripts have similar functions for raising
and lowering volume and brightness. They both
will send notifications to the user of the current
corresponding value. I made them for use with
function keys in the window managers I use.
--up/--down for both, and --mute is also available
for the volume script. volume uses pamixer and
brightness uses light.
```