# Package

version       = "0.1.0"
author        = "Will Reed"
description   = "process killer"
license       = "BSD-2-CLAUSE"
srcDir        = "src"
bin           = @["signim"]


# Dependencies

requires "nim >= 1.6.6"