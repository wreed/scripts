import posix_utils;
import std/strutils
import osproc;
import os;

proc printf(text: string) =
  stdout.write(text);

var argv: seq[string] = commandLineParams();

proc precheck(): string =
  case argv.len:
  of 0:
    stdout.write("You didn't give any arguments.\n");
  of 1:
    stdout.write("killing: " & argv[0] & "\n");
  else:
    stdout.write("Too many args.\n");
  result = argv[0];
  discard argv;

proc main(name: string) =
  const sigseq: seq[int] = @[15, 2, 1, 9];
  for s in 0..(sigseq.len - 1):
    var pidof = execCmdEx("pidof " & name);
    if (pidof[1] == 0):
      removeSuffix(pidof.output);
      var procpid = int32(parseInt(pidof.output));
      sendSignal(procpid, sigseq[s]);
    else:
      printf(name & " not found in process table.\n");
      break;
  discard sigseq;
  discard name;

main(precheck());
